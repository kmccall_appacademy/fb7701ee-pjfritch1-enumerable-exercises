require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  arr.reduce(0) {|acc, el| acc + el}
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.each do |str|
    if not substring?(str, substring)
      return false
    end
  end

  true
end

def substring?(string,substring)
  string.include?(substring)
end


# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
letters = string.delete(" ").chars.sort.select {|chr| string.count(chr) > 1}
letters.uniq
end

# Alternate method body:
# letters = []
# string.delete(" ").each_char do |chr|
#   if string.count(chr) > 1 && !(letters.include?(chr))
#     letters << chr
#   end
# end
# letters.sort


# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  sorted_words = string.split.sort_by {|word| word.length}
  sorted_words[-2..-1].reverse
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  letters = %w(a b c d e f g h i j k l m n o p q r s t u v w x y z)
  letters.reject {|ltr| string.include?(ltr)}
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  (first_yr..last_yr).select {|yr| yr if not_repeat_year?(yr)}
end

def not_repeat_year?(year)
  a_string = year.to_s
  a_string.each_char do |chr|
    return false if a_string.count(chr) > 1
  end

  true
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  wonders = songs.select {|song| song if no_repeats?(song, songs)}
  wonders.uniq
end

def no_repeats?(song_name, songs)
  songs.each_with_index do |song, i|
    if song == song_name
      return false if songs[i - 1] == song_name || songs[i + 1] == song_name
    end
  end

  true
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  words = string.split
  words.each {|word| remove_punctuation(word)}
  c_words = words.reject {|wrd| c_distance(wrd).is_a?(String)}
  c_words.sort_by! {|w| c_distance(w)}
  c_words[0]
end

def c_distance(word)
  word.reverse.each_char.with_index do |chr, i|
    return i if chr.downcase == "c"
  end
end

def remove_punctuation(word)
  word.gsub!(/\W/,"")
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  all_ranges = []
  range = []
  arr.each_with_index do |num, i|
    if (num != arr[i - 1] || i == 0) && num == arr[i + 1]
      range = [i]
    elsif num == arr[i - 1] && num != arr[i + 1]
      range << i
      all_ranges << range
    end
  end

  all_ranges
end
